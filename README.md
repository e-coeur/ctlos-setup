# ctlos-setup


## Download ctlos

Install [ctlos-bspwm](https://ctlos.github.io/get/)

## Setup

### 0. Connect to wifi through terminal: 

```
nmcli dev wifi connect <wifi spot name> password <real password>
```

### 1. Download configs onto your PC:
```
git clone https://gitlab.com/e-coeur/ctlos-setup.git
cd ctlos-setup
```

### 2. Install programs from pkglist pacman & yay:
```
sudo pacman -S - < pkglist.txt
yay -S --needed - < pkglist_aur.txt
```
INFO 
- Pulseaudio asks for pulseaudio-control.bash before. [source](https://github.com/marioortizmanero/polybar-pulseaudio-control). Config file was downloaded from here [here](https://github.com/marioortizmanero/polybar-pulseaudio-control/releases/latest)
- Check for virtualenvwrapper [source](https://virtualenvwrapper.readthedocs.io/en/latest/install.html)


### 3. Install powerlevel10k [source](https://github.com/romkatv/powerlevel10k)
```
```

### 4.1 Edit zshrc:
```
vim ~/.zshrc
```

Changes:
```
# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

...

export LC_TIME=en_GB.utf8

...

export WORKON_HOME=$HOME/.virtualenvs   # Optional
export PROJECT_HOME=$HOME/projects      # Optional
# TODO: fix source ~/.local/bin/virtualenvwrapper.sh

export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm"  printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

export VIRTUALENVWRAPPER_PYTHON=$(which python3)

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]]  source ~/.p10k.zsh
```

### 4.2 Edit bspwmrc:
```
vim ~/.config/bspwm/bspwmrc
```

Changes:
```
do
  bspc config -d ${SPACE} window_gap      0
  bspc config -d ${SPACE} top_padding     0
  bspc config -d ${SPACE} bottom_padding  0
  bspc config -d ${SPACE} left_padding    0
  bspc config -d ${SPACE} right_padding   0
done

...

# Rules
bspc rule -a kitty:Tiled desktop='^1' follow=on
bspc rule -a kitty:Tmux desktop='^1' follow=on
# bspc rule -a google-chrome-stable desktop='^2'
bspc rule -a firefox desktop='^2'
bspc rule -a Thunar desktop='^5' follow=on focus=on
bspc rule -a Thunar:Dialog state=floating center=true
bspc rule -a TelegramDesktop desktop='^4' follow=on
# bspc rule -a Slack desktop='^4' follow=on
bspc rule -a discord desktop='^4' follow=on
# bspc rule -a zoom desktop='^6' follow=on
# bspc rule -a Skype desktop='^6' follow=on

... 

bspc rule -a kitty:Newsboat state=floating center=on
bspc rule -a kitty:Ranger state=floating center=on
bspc rule -a kitty:Pacui state=floating center=on
bspc rule -a kitty:Float state=floating
bspc rule -a kitty:Draw state=floating
```

### 4.3 Edit autostart.sh:
```
vim ~/.config/bspwm/autostart.sh
```

Changes:
```
run telegram-desktop &
run slack &
run firefox &
```

### 4.4 Edit sxhkdrc: 
```
vim ~/.config/bspwm/sxhkd/sxhkdrc
```

Changes:
```
# terminal emulator Tiled
super + Return
  kitty

# terminal emulator Float
alt + t
  $HOME/.config/bspwm/bspwmfloat kitty

...

# backlight control
super + F1 
  xbacklight -inc 10 
super + F2 
  xbacklight -dec 10
```

### 4.5 Edit polybar config:
```
vim ~/.config/bspwm/polybar/config

```

Changes: copy paste everything.


### 5. Setup CUPS
```
sudo systemctl cups
sudo systemctl status -l cups
find "Manage printing" (http://localhost:631/)
```

### 6. Install nerd-fonts
```
git clone https://github.com/ryanoasis/nerd-fonts
cd nerd-fonts
sudo ./install.sh
```

### 6. Fix time
```
timedatectl set-ntp true
```

### 7. Use vim with sudo:
```
usermod -G wheel e-coeur
sudo nano /etc/sudoers
%wheel ALL=(ALL) ALL
```


### 10. Setup OpenVPN
```
sudo nmcli connection import type openvpn file ~/Google\ Drive/Common/Passes/1w.ovpn
```
Then add login and pass through NetworkManager.

### 11. Setup screen resolution: 
```
sudo nvim /usr/share/sddm/script/Xsetup
```
add this line: 
```
xrandr --output eDP --mode 1920x1080 --rate 120.00
```

### 12. Setup wall with nitrogen (add image to /usr/share/wall/), then choose it in Nitrogen.

### 13. Setup Neovim config: 
```
mkdir ~/.config/nvim 
mv init.vim ~/.config/nvim/init.vim
```
