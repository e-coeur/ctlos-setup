" this will install vim-plug if not installed
if empty(glob('~/.config/nvim/autoload/plug.vim'))
    silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall
endif
    
call plug#begin()
Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'} " this is for auto complete, prettier and tslinting
Plug 'jiangmiao/auto-pairs' " auto brackets
Plug 'yuezk/vim-js'
Plug 'HerringtonDarkholme/yats.vim'
Plug 'maxmellon/vim-jsx-pretty'
Plug 'vim-airline/vim-airline' " more informative bar
Plug 'vim-airline/vim-airline-themes' " bar theme
Plug 'scrooloose/nerdcommenter' " comment code <leader>cc, uncomment <leader>cu, <leader> 
Plug 'sbdchd/neoformat' " format Python with yapf
Plug 'davidhalter/jedi-vim' " go-to, <leader>d - to definition, k - check doc, <leader>n - show usage of a name in file, <leader>r - rename name
Plug 'scrooloose/nerdtree' " files and exploration, ctrl+K+ctrl+B to toggle
Plug 'neomake/neomake' " code checker
Plug 'terryma/vim-multiple-cursors' " ctrl + N add cursor to next occurence, ctrl + X skip next occurence
Plug 'machakann/vim-highlightedyank'
Plug 'tmhedberg/SimpylFold' " zo open fold, zO open fold and subfold, zc close fold, zC close fold and subfold
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'wokalski/autocomplete-flow'
Plug 'Shougo/neosnippet'
Plug 'Shougo/neosnippet-snippets'
Plug 'deoplete-plugins/deoplete-jedi'

" code check 
let g:coc_global_extensions = ['coc-tslint-plugin', 'coc-tsserver', 'coc-css', 'coc-html', 'coc-json', 'coc-prettier']  " list of CoC extensions needed

" bar theme
let g:airline_theme='bubblegum'

" file exploration hotkey 
noremap <silent> <C-k><C-B> :NERDTreeToggle<CR>

" disable autocompletion, because we use deoplete for completion
let g:jedi#completions_enabled = 0

" open the go-to function in split, not another buffer
let g:jedi#use_splits_not_buffers = "right"
let g:neomake_python_enabled_makers = ['pylint', 'eslint']
 
let g:deoplete#enable_at_startup = 1
let g:neosnippet#enable_completed_snippet = 1

let g:loaded_perl_provider = 0
let g:loaded_ruby_provider = 0

set number

call plug#end()


syntax enable
colorscheme monokai
